﻿# Change Log
All notable changes to this project will be documented in this file.

## [0.0.1] - 2017-10-10
### Added
- This CHANGELOG file
- The README file
- Initial Singleton version