﻿using UnityEngine;

namespace VersionZero.UnityAssets {

    /// <summary>
    /// Be aware this will not prevent a non singleton constructor
    ///   such as `T myT = new T();`
    /// To prevent that, add `protected T () {}` to your singleton class.
    /// </summary>
    public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour {

        #region Protected

        protected static T _instance;
        protected static object _lock = new object();

        #endregion

        #region Static

        public static T instance {
            get {
                lock (_lock) {
                    if(!_instance) {
                        _instance = (T)FindObjectOfType(typeof(T));

                        if(FindObjectsOfType(typeof(T)).Length > 1) {
                            if(Application.isEditor)
                                Debug.LogError(string.Concat("[Singleton] Something went really wrong ", " - there should never be more than 1 singleton!", " Reopening the scene might fix it."));
                            return _instance;
                        }

                        if(_instance == null) {
                            GameObject singleton = new GameObject();
                            _instance = singleton.AddComponent<T>();
                            singleton.name = string.Concat("(singleton) ", typeof(T).ToString());

                            DontDestroyOnLoad(singleton);

                            if(Application.isEditor)
                                Debug.LogWarning(string.Concat("[Singleton] An instance of ", typeof(T), " is needed in the scene, so '", singleton, "' was created with DontDestroyOnLoad."));
                        }
                        else {
                            if(Application.isEditor)
                                Debug.LogWarning(string.Concat("[Singleton] Using instance already created: ", _instance.gameObject.name));
                        }
                    }

                    return _instance;
                }
            }
        }

        #endregion

    }

}